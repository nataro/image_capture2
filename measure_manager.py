import logging
import threading
from abc import ABCMeta, abstractmethod
from datetime import datetime
from pathlib import Path
from typing import Sequence, Optional, Dict

import numpy as np

import cv2

from measure.measure import CallableMeasurement

from utils.ar_clipper import ArClipper
from ml.dl_model import Fai2Classifier, AlbumentationsTransform

# from .path_tools import FileSave
# from .measure.measure import CallableMeasurement, PicoData, PicoDataH5FileSave

# ライブラリ側でのロギング設定
logger = logging.getLogger(__name__)


class RecordManagerBuilder(metaclass=ABCMeta):

    def __init__(self, device: CallableMeasurement):
        self._device = device
        self._event = threading.Event()  # 計測処理に関するスレッドを同期して開始するために使用
        self._lock = threading.Lock()  # 複数のスレッドが同時に録音デバイスにアクセスしないようにロックする

    def device_open(self) -> bool:
        return self._device.open()

    def device_close(self) -> None:
        self._device.close()

    def recording_thread_manage(self,
                                dir_path: str,
                                file_name: str,
                                received: dict) -> None:
        """
        計測用のスレッドを管理する

        Parameters
        ----------
        # device : CallableMeasurement
        dir_path : str
        file_name : str
        received : dict
            マシニングセンタからシリアル通信で受け取った情報
        """
        record_t = threading.Thread(
            name=self.__class__.__name__ + '-record_thread',
            target=self.record_via_device,
            # args=(event, lock, device, dir_path, file_name, received)
            args=(self._event, self._lock, dir_path, file_name, received)
        )
        record_t.start()
        # 複数種類のスレッドを同時に実行させたいことがあるかもしれないので、event.set()でタイミングをあわせる予定
        # event.set()  # event.wait()で待機しているプロセスを全部同時に解除
        self._event.set()  # event.wait()で待機しているプロセスを全部同時に解除

    @abstractmethod
    def record_via_device(self,
                          event: threading.Event,
                          lock: threading.Lock,
                          dir_path: str,
                          file_name: str,
                          received: dict,
                          ext: str):
        pass

    @abstractmethod
    def check_dict(self, received: Dict) -> str:
        pass

    @staticmethod
    def keys_check(target_keys: Sequence, must_keys: Sequence) -> bool:
        """
        target_keysの中に、must_keysが全部含まれているか確認する
        """
        return set(target_keys) >= set(must_keys)


class CaptureManagerBuilder(RecordManagerBuilder):
    """
    usbカメラ等を用いて画像を撮影する

        * 取得した画像をマーカを基準として指定した範囲で切り取る
            * 切り取りのツールはutils.ar_clipper.ArClipperを用いる
                ArClipperには、切り取りマーカーや切り取り範囲の設定などが含まれる
    """

    def __init__(self, device: CallableMeasurement, ar_clipper=ArClipper()):
        super().__init__(device)
        self._ar_clipper = ar_clipper
        # self._classifier = Fai2Classifier(path='./ml/trained_model.pkl', size=128)
        model_path = '/home/seisan/prg/image_capture2/ml/trained_model.pkl'
        self._classifier = Fai2Classifier(path=model_path, size=128)

    def ar_clipped_image(self, img):
        """
        utils.ar_clipper.ArClipperでマーカを基準として指定した範囲での切り取りを行う
        ArClipperには、切り取りマーカーや切り取り範囲の設定などが含まれる
        :param img:
        :return:
        """
        if img is None:
            return None
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # マーカーボードを検出し、そのrotation vectorとtranslation vectorを返す
        retval, rvec_org, tvec_org = self._ar_clipper.estimate_board(gray)

        if retval != 0:  #
            # clippingする範囲の画像座標（ROI）を取得する
            roi = self._ar_clipper.roi_coordinates(rvec_org, tvec_org)

            # 画像を切り取る
            cliiped_img = self._ar_clipper.image_clipping(img, roi)

            return cliiped_img

        return None

    def record_via_device(self,
                          event: threading.Event,
                          lock: threading.Lock,
                          dir_path: str,
                          file_name: str = '',
                          received: dict = None,
                          ext: str = 'jpg'):
        """
        Parameters
        ----------
        event : threading.Event
        lock : threading.Lock
        dir_path : str
        file_name : str
        received : dict
            マシニングセンタからシリアル通信で受け取った情報
        ext : str
        """
        logging.debug(self.__class__.__name__ + ' : record thread start')
        event.wait()  # event.set()が実行されるまで待機
        logging.debug(self.__class__.__name__ + ' : do event')
        with lock:  # deviceへのアクセスを制限
            logging.debug(self.__class__.__name__ + ' : lock')

            # ここで画像取得
            data = self._device()
            # armarkerでクリップする
            data = self.ar_clipped_image(data)
            # 矩形画像の場合、正方形にpaddingする
            data = self.square_padding(data)
            # クラス分類結果を文字列で得る
            c_name = self._classifier(data)

            # 　ファイル名の先頭にクラス名を追加
            dt = datetime.now()
            file_path = dir_path + "/{0}_{1}.".format(c_name, dt.strftime("%Y%m%d-%H%M%S-%f_") + file_name) + ext

            # ディレクトリ内に同じクラス名のファイルが何個あるか確認する
            file_num = self.get_files_num(dir_path, str(c_name))

            # 同じクラスのファイルは個数を確認して保存するかどうか判断する
            file_num_limit = 50
            if (data is not None) and (file_num < file_num_limit):
                cv2.imwrite(file_path, data)

        logging.debug(self.__class__.__name__ + ' : unlock')
        # logging.debug(self.__class__.__name__ + ' : record end')

    def check_dict(self, received: Dict) -> Optional[str]:
        """
        received = {'AC': 10.0,'ST': 5.0, 'ZP': -0.08}
        というような辞書型を受け取って、データを保存するかどうか判断する
        データを保存する場合はself.recording()を呼び出す

        Parameters
        ----------
        received : dict
            e.g. {'AC': 10.0,'ST': 5.0, 'ZP': -0.08}

        """
        must_keys = ('CC',)
        is_involve = self.keys_check(received.keys(), must_keys)
        if not is_involve:
            return None

        counts = received.get('CC')  # 穴加工数を'CC'というkeyで受け取る予定
        try:
            counts = int(counts)
        except (ValueError, TypeError) as e:
            logging.error(e)
            # print('loggin check value error')
        else:
            file_name = "cc{0}".format(counts)
            return file_name
        return None

    @staticmethod
    def get_files_num(dir_path, file_name: str) -> int:
        """
        ディレクトリに含まれるファイルの内、ファイル名の一部が一致するファイルの個数をカウントする
        Parameters
        ----------
        dir_path:検索対象となるディレクトリ
        file_name:検索するファイル名の一部

        Returns
        -------
        該当するファイルの個数
        """
        path_ob = Path(dir_path)
        file_name_list = [str(p) for p in path_ob.glob('*')]
        # 取得した各ファイル名を指定した文字でスプリットし、その要素数をカウントしていく
        # 該当する文字が含まれるファイル名は2以上となり、該当しない場合は1となる
        sep_cont_list = [len(f.split(file_name)) for f in file_name_list]
        return sum(sep_cont_list) - len(file_name_list)

    @staticmethod
    def square_padding(img: np.ndarray) -> np.ndarray:
        """
        正方形の画像にパディングする
        Parameters
        ----------
        img

        Returns
        -------

        """
        mean = (0.485, 0.456, 0.406)
        value = [int(i * 255) for i in mean]
        h, w, *c = img.shape
        sq_size = max([h, w])
        top = (sq_size - h) // 2
        bottom = sq_size - h - top
        left = (sq_size - w) // 2
        right = sq_size - w - left

        sq_img = cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT, value=value)

        return sq_img


class RecordManagerDirector(object):
    def __init__(self, builder: RecordManagerBuilder, dir_path: str = None):
        self._builder = builder
        self._storage_path = self.create_storage_directory(dir_path)

    def __call__(self, *args, **kwargs):
        """
        __call__()でメインの動作ができるようにしておく
        """
        (received_dict,) = args
        # if not any(received_dict):
        if any(received_dict):  # dictが空かどうかのチェック
            # print(received_dict)
            logger.debug(self.__class__.__name__ + ': __call__() received : ', received_dict)
            file_name = self._builder.check_dict(received_dict)
            if file_name is not None:
                self.recording(file_name, received_dict)

    def device_open(self) -> bool:
        return self._builder.device_open()

    def device_close(self) -> None:
        self._builder.device_close()

    def recording(self, file_name: str = '', received: dict = None) -> None:
        self._builder.recording_thread_manage(self._storage_path, file_name, received)

    @staticmethod
    def create_storage_directory(dir_path: str = None) -> str:
        """
        インスタンスを生成した時点での/年/月/日という階層のディレクトリを生成する
        Parameters
        ----------
        dir_path : str
            配置するディレクトリ
            （指定しない場合はカレントディレクトリになる）

        Returns
        -------
        str
            生成したディレクトリのパス
            （配置するディレクトリは自動生成される）

        """
        p_dir_path = Path(dir_path) if dir_path else Path.cwd()

        dt = datetime.now()
        y = dt.strftime('%Y')
        m = dt.strftime('%m')
        d = dt.strftime('%d')

        p_dir_path = p_dir_path / y / m / d

        # ディレクトリを生成
        # exist_ok : 既に存在していてもエラーにならない
        # parents : 階層が深くても再帰的に生成
        p_dir_path.mkdir(exist_ok=True, parents=True)

        return str(p_dir_path.resolve())


if __name__ == "__main__":
    """ 動作確認用
    measure_manager.pyを単体で実行する場合は、
    Terminalでserial_listenerの位置にいる状態から「-m」オプションで実行しないといけない
    （相対インポートをしているので「.py」をつけずにモジュールとして実行）
    python -m util.measure_mamager

    ちなみに、下記のように*.pyファイルとして実行すると相対パスで指定したモジュールを
    Importできずにエラーとなる
    python util\measure_manager.py

    ImportError: attempted relative import with no known parent package

    参考
    https://note.nkmk.me/python-relative-import/
    """

    import time
    from measure.cam_capture import CaptureDevice

    cap_dev = CaptureDevice(cam_num=0)
    cm = CaptureManagerBuilder(cap_dev)
    dir_path = '/mnt/hdd/satahdd'
    cd = RecordManagerDirector(cm, dir_path=dir_path)

    # ダミーの受信データ
    # rec_dict = {'AC': 10.0, 'ST': 5.0, 'ZP': -0.08, }
    rec_dict = {'CC': 10.0, }

    cd.device_open()  # DataManagerが保持してるデバイスを開いて計測準備を行う

    cd(rec_dict)  # 計測の実行

    time.sleep(15)  # 計測が実行される前にcloseするとエラーになるので待機

    print('close')
    cd.device_close()  # DataManagerが保持してるデバイスを閉じる
