# import dataclasses
from abc import ABCMeta, abstractmethod
# from functools import partial
# from pathlib import Path

# import numpy


class CallableMeasurement(metaclass=ABCMeta):
    @abstractmethod
    def __call__(self, **kwargs):  # data_load
        """Retrieve data from the input source and return an object."""
        return

    @abstractmethod
    def open(self):
        """Open the measurement device."""
        pass

    @abstractmethod
    def close(self):
        """Close the measurement device.
        """
        pass