from typing import Optional

import cv2
import numpy as np

from measure.measure import CallableMeasurement


class CaptureDevice(CallableMeasurement):

    def __init__(self, cam_num: int = 0):
        self._cam_num = cam_num
        self._video_cap = None
        self._is_open = False

    def __call__(self, **kwargs) -> Optional[np.ndarray]:
        if not self._is_open:
            self.open()
        if self._is_open:
            ret, frame = self._video_cap.read()
            if ret:
                return frame
        return None

    def open(self) -> bool:
        return self.open_cam()

    def close(self) -> None:
        return self.close_cam()

    def cam_capture(self) -> cv2.VideoCapture:
        # cap = cv2.VideoCapture(self._cam_num, cv2.CAP_DSHOW)
        cap = cv2.VideoCapture(self._cam_num)
        return cap

    def open_cam(self):
        self._video_cap = self.cam_capture()
        self._is_open = False if self._video_cap is None else self._video_cap.isOpened()
        return self._is_open

    def close_cam(self) -> None:
        if self._video_cap:
            self._video_cap.release()
            cv2.destroyAllWindows()  # Handles the releasing of the camera accordingly
            self._is_open = False


