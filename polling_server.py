import time
import logging

from tornado.ioloop import IOLoop

from measure.cam_capture import CaptureDevice
from measure_manager import RecordManagerDirector, CaptureManagerBuilder

logging.basicConfig(level=logging.DEBUG, format='%(threadName)s: %(message)s')

"""
PollingServerが定期的にself._cap_director(RecordManagerDirector)を動作させるようになっている
その際、辞書型のデータを渡しているが、今回はこれは不要なダミーデータである

CaptureManagerBuilderクラスで
    * DLモデルの読み込みファイル指定
    * 保存する画像の個数制限
    * クリッピングした画像の正方形パディングを行っている

また、ARマーカーを利用したクリッピングは、utils.ar_clipper.pyのArClipperクラスに直接切り取り範囲を指定して書いている

画像データの保存場所は、PollingServerのインスタンス生成時に渡す,self._cap_director(RecordManagerDirector)
を生成する際に指定すれば良い。

"""


class PollingServer(object):
    """
    """

    def __init__(self,
                 polling_hz: float = 1,
                 cap: RecordManagerDirector = None,
                 ):
        """
        Parameters
        ----------
        polling_hz : float
            ポーリングの実行周期[hz]
        """
        self._io_loop = IOLoop.current()
        self._loop_freq = polling_hz

        # データロギングを管理するクラス
        self._cap_director = cap

        self.loop()  # 定義したループ処理を実行

    def __enter__(self):
        """
        with 文開始時にコールされる。
        ここで返す値なりオブジェクトを with 文の as エイリアスで受けることができる
        """
        # self._pico_director.device_open()
        # カメラを開く前に少しタイムラグを置かないとエラーになる
        time.sleep(5)
        self._cap_director.device_open()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        exc_type, exc_value, traceback は with 文内で例外が発生した際に例外情報を受取る
        例外が発生しないときはすべて None がセットされる
        """
        # self._pico_director.device_close()
        self._cap_director.device_close()

    def loop(self):
        """再帰的に自分自身を呼び出してループ処理"""
        self._io_loop.call_later(1 / self._loop_freq, self.loop)  # こっちでもよさそう
        self.callback()

    def callback(self, receive_segment: bytes = None):
        """
        Parameters
        ----------
        receive_segment : bytes

        """
        dummy_rec_dict = {'CC': 0.0, }
        self._cap_director(dummy_rec_dict)


if __name__ == "__main__":
    #
    from ml.dl_model import AlbumentationsTransform

    cap_dev = CaptureDevice(cam_num=-1)
    cm = CaptureManagerBuilder(cap_dev)
    dir_path = '/mnt/hdd/satahdd'
    cd = RecordManagerDirector(cm, dir_path=dir_path)

    with PollingServer(polling_hz=1 / 30, cap=cd) as sp:
        # windows の場合は(Ctrl+PauseもしくはCtrl+C)で止まる
        try:
            IOLoop.instance().start()
        except KeyboardInterrupt:
            IOLoop.instance().stop()
